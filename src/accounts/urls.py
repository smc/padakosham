from django.conf.urls import include, url
#from registration.backends.simple.views import RegistrationView
from cuser.forms import AuthenticationForm
from django.contrib.auth.views import login
from .forms import UserForm
from  accounts import regbackend
from .views import registration_redirect
urlpatterns = [
    # Other URL patterns ...
    url(r'^register/$', registration_redirect.as_view(form_class=UserForm), name='register'),
    url(r'^login/$', login, {'authentication_form': AuthenticationForm}, name='login'),
    url(r'^', include('registration.backends.simple.urls')),
    # More URL patterns ...
]
