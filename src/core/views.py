import random

from django.views import View
from django.views.generic import FormView, TemplateView
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.forms import formset_factory

from cuser.models import CUser

from accounts.models import StudentProfile
from institutions.models import Institution

from .forms import WordAddForm, StudentAddForm, WordMeaningAddForm
from .models import Word, Place
from .words import get_word_objects

class HomeView(TemplateView):
    template_name = 'apps/UI_test/home.html'

class WordsReviewView(View):
    def get(self, request):
        unverified_words = Word.objects.filter(worddetails__is_verified=False)
        context = {
            'unverified_words': unverified_words
        }
        return render(request, "apps/core/review.html", context=context)

class SuccessView(View):
    def get(self, request):
        unverified_words = Word.objects.filter(worddetails__is_verified=False)
        context = {
            'unverified_words': unverified_words
        }
        return render(request, "apps/core/result.html", context=context)

class WordsEditView(View):
    def get(self, request, pk):
        return HttpResponse("LOL!")


class WordsApproveView(View):
    def post(self, request, pk):
        word = Word.objects.get(id=pk)
        word.worddetails.is_verified = True
        word.worddetails.verified_by = request.user
        word.worddetails.save()
        return redirect('review')


class StudentsAddView(FormView):
    template_name = 'apps/core/students_add.html'
    form_class = StudentAddForm

    def post(self, *args, **kwargs):
        name = self.request.POST.get('name')
        institution = Institution.objects.get(id=int(self.request.POST.get('school')))
        user = CUser()
        user.first_name = name
        user.email = name + str(random.randint(0, 100000)) + "@itschool.in"
        user.save()
        profile = StudentProfile()
        profile.institution = institution
        profile.grade = self.request.POST.get('grade')
        profile.user = user
        profile.user_id = user.id
        profile.save()
        self.request.session['student_id'] = user.id
        return redirect('/home/words/add/')


class WordsAddView(FormView):
    template_name = 'apps/core/words_add.html'
    form_class = WordAddForm
    form_set_class = WordMeaningAddForm

    def get_form_set(self):
        form_set_class = self.form_set_class
        kwargs = {}
        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        MeaningFormSet = formset_factory(
            form_set_class, extra=20,
            min_num=1, validate_min=True
        )
        return MeaningFormSet(**kwargs)

    def form_valid(self, form, form_set):
        word = form.save()

        homonyms_csv = form.cleaned_data['homonyms']
        if homonyms_csv:
            word.homonyms.add(*get_word_objects(homonyms_csv))

        place_name = form.cleaned_data['place']
        if place_name:
            place = Place(name=place_name)
            place.save()
            word.place = place

        plural_text = form.cleaned_data['plural']
        if plural_text:
            word.plural = get_word_objects(plural_text)[0]

        singular_text = form.cleaned_data['singular']
        if singular_text:
            word.plural = get_word_objects(singular_text)[0]

        root_text = form.cleaned_data['root']
        if root_text:
            word.plural = get_word_objects(root_text)[0]

        word.save()

        for f in form_set:
            meaning = f.save(commit=False)
            meaning.word = word
            meaning.save()

            synonyms_csv = f.cleaned_data.get('synonyms')
            if synonyms_csv:
                meaning.synonyms.add(*get_word_objects(synonyms_csv))

            opposites_csv = f.cleaned_data.get('opposites')
            if opposites_csv:
                meaning.opposites.add(*get_word_objects(opposites_csv))

            meaning.save()




        add_another = self.request.POST.get('add_another')
        finish = self.request.POST.get('finish')

        if finish:
            return redirect('/home/words/success/')
        else:
            return redirect('/home/words/add/')

    def form_invalid(self, form, form_set):
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form_set = self.get_form_set()
        if form.is_valid() and form_set.is_valid():
            return self.form_valid(form, form_set)
        else:
            return self.form_invalid(form, form_set)

    def get_context_data(self, *args, **kwargs):
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()
        if 'formset' not in kwargs:
            kwargs['formset'] = self.get_form_set()
        return super().get_context_data(*args, **kwargs)
        return context
