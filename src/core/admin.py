from django.contrib import admin
from .models import Category,Language,Place,Word,WordDetails,Meaning
# Register your models here.

admin.site.register(Category)
admin.site.register(Language)
admin.site.register(Place)
admin.site.register(Word)
admin.site.register(WordDetails)
admin.site.register(Meaning)
