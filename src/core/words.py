from .models import Word


def get_word_objects(words_csv):
    words = words_csv.replace(' ', '').split(',')
    # instances created with bulk_create throw an error when saved
    # to a M2M field
    return [
        Word.objects.create(text=w) for w in words
    ]