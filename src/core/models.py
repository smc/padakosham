from django.db import models
from django.conf import settings

from utils.models import TimestampedBaseModel


User = settings.AUTH_USER_MODEL


class Category(models.Model):
    """
    Categories of words like adjective, noun, etc.
    """
    name = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % self.name


class Language(models.Model):
    """
    The language code and its human readable form.
    """
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % self.name


class Place(models.Model):
    """
    Places info.
    """
    name = models.CharField(max_length=100)

    def __str__(self):
        return '%s' % self.name


class Word(TimestampedBaseModel):
    """
    Master table for the word text and related metadata.
    """
    text = models.CharField(max_length=50, verbose_name='വാക്ക്')
    etymology = models.TextField(null=True, blank=True,verbose_name='നിഷ്പത്തി')
    homonyms = models.ManyToManyField(
        'self',
        blank=True,
        related_name='homonyms',
        verbose_name='നാനാർത്ഥം'
    )
    is_dialect = models.BooleanField(default=False,verbose_name='പ്രാദേശികഭേദമാണോ?')
    origin_language = models.ForeignKey(Language, null=True, blank=True,related_name='originated_words',verbose_name='ഏതുഭാഷയിൽ നിന്നും വന്നു?')
    language = models.ForeignKey(Language, null=True, blank=True,verbose_name='ഭാഷ')
    place = models.ForeignKey(
        Place,
        null=True,
        blank=True,
        related_name="places",
        verbose_name='പ്രദേശം'
    )
    is_plural = models.BooleanField(default=False,verbose_name='ബഹുവചനമാണോ?')
    plural = models.OneToOneField(
        'self',
        null=True,
        blank=True,
        related_name='plural_of',
        verbose_name='ബഹുവചനം'
    )
    root = models.ForeignKey(
        'self', null=True, blank=True, related_name='derived_words', verbose_name='മൂലപദം')
    singular = models.OneToOneField(
        'self', null=True, blank=True, related_name='singular_of',verbose_name='ഏകവചനം')

    def __str__(self):
        return '%s' % self.text


class WordDetails(TimestampedBaseModel):
    """
    Contains info about Word entry and manipulation.
    """
    word = models.OneToOneField(Word)
    created_by = models.ForeignKey(User, related_name="words_created")
    is_verified = models.BooleanField(default=False)
    verified_by = models.ForeignKey(
        User,
        null=True,
        blank=True,
        related_name="words_verified"
    )
class Meaning(models.Model):
    MASCULINE = 'M'
    FEMININE = 'F'
    NEUTRAL = 'N'
    GENDER_CHOICES = (
        (MASCULINE, 'പുല്ലിംഗം',),
        (FEMININE,'സ്ത്രീലിംഗം ',),
        (NEUTRAL, 'നപുംസകലിംഗം',),
    )
    word = models.ForeignKey(Word)
    text = models.TextField(
        verbose_name='അർത്ഥം'
    )
    gender = models.CharField(
        max_length=1,
        choices=GENDER_CHOICES,
        default=NEUTRAL,
        verbose_name='ലിംഗം')
    category = models.ForeignKey(
        Category,
        null=True,
        blank=True,
        verbose_name='പദവിഭാഗം'
    )
    examples = models.TextField(null=True, blank=True,verbose_name='ഉദാഹരണം')
    synonyms = models.ManyToManyField(
        Word,
        blank=True,
        related_name="synonyms",
        verbose_name='പര്യായങ്ങൾ'
    )
    opposites = models.ManyToManyField(
        Word,
        blank=True,
        related_name='opposites',
        verbose_name='വിപരീതം'
    )
    # translation = models.ManyToManyField(
    #     'self',
    #     blank=True,
    #     related_name="translations",
    #     verbose_name='പരിഭാഷകൾ'
    # )

    def __str__(self):
        return '%s' % self.text
